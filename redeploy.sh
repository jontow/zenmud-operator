#!/bin/sh

# Set this to any non-blank value to avoid loading secrets
# Set this to blank value to load secrets from ~/.docker/config.json
PRIVATE_REGISTRY="${PRIVATE_REGISTRY:-no}"

COLOR_RED="[1;31m"
COLOR_GREEN="[1;32m"
COLOR_END="[0;0m"

################################################################################
# Sanity checks, if applicable

if [ -z "${PRIVATE_REGISTRY}" ] && ! oc get secret regcred; then
    echo "${COLOR_RED}WARNING: Missing 'regcred' dockerhub secret, trying to auto-add:${COLOR_END}"
    if ! oc create secret generic regcred \
      --from-file=.dockerconfigjson="${HOME}/.docker/config.json" \
      --type=kubernetes.io/dockerconfigjson; then
        echo "${COLOR_RED}ERROR: Could not add dockerhub secret.${COLOR_END}"
        exit 1
    fi
    echo "${COLOR_GREEN}Secret 'regcred' added from existing docker config.json${COLOR_END}"
fi

################################################################################

# Tear it all down
if [ ! -z "$(oc get deployments/zenmud-operator 2>/dev/null)" ]; then
    echo "${COLOR_RED}Tearing resources down..${COLOR_END}"
    oc delete -f deploy/crds/mud.zenbsd.net_v1alpha1_zenmud_cr.yaml
    oc delete -f deploy/operator.yaml
    oc delete -f deploy/role_binding.yaml
    oc delete -f deploy/role.yaml
    oc delete -f deploy/service_account.yaml
    oc delete -f deploy/crds/mud.zenbsd.net_zenmuds_crd.yaml
fi

while [ ! -z "$(oc get pods 2>/dev/null && sleep 2)" ]; do
    echo "${COLOR_RED}Waiting for resources to terminate..${COLOR_END}"
done
#exit
echo "${COLOR_GREEN}Resources terminated, building up..${COLOR_END}"

################################################################################

# Build it all back up
#operator-sdk build "${IMAGE}"
#docker push "${IMAGE}"

oc create -f deploy/crds/mud.zenbsd.net_zenmuds_crd.yaml
oc create -f deploy/service_account.yaml
oc create -f deploy/role.yaml
oc create -f deploy/role_binding.yaml
oc create -f deploy/operator.yaml
oc create -f deploy/crds/mud.zenbsd.net_v1alpha1_zenmud_cr.yaml

################################################################################

while ! oc get pods -l name=zenmud-operator | grep -q Running ; do
    echo "${COLOR_GREEN}Waiting for pods to start..${COLOR_END}"
    sleep 2
done
echo "${COLOR_GREEN}Tailing ansible log, ^C to exit${COLOR_END}"
oc logs -f deployments/zenmud-operator -c ansible
