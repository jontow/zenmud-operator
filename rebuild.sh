#!/bin/sh

if [ -z "$1" ]; then
    echo "Syntax: rebuild.sh <version>"
    exit 1
fi

version="$1"

# registry_pub_url needs to contain a trailing / if non-empty
#registry_pub_url="registry.cloudapps-dev.unc.edu:443/"

registry_pub_url="${registry_pub_url:-}"
project="${project:-jontow}"
img_name="${img_name:-zenmud-operator}"
full_img="${project}/${img_name}"

################################################################################

operator-sdk build "${full_img}":"${version}" || exit 1

if [ ! -z "${registry_pub_url}" ]; then
    docker tag \
      "${full_img}":"${version}" \
      "${registry_pub_url}""${full_img}":"${version}" \
      || exit 1

    docker push "${registry_pub_url}""${full_img}":"${version}" || exit 1

    oc tag \
      "${img_name}":"${version}" \
      "${img_name}":latest \
      || exit 1
else
    docker push "${full_img}":"${version}"

    docker tag \
      "${full_img}":"${version}" \
      "${full_img}":latest \
      || exit 1

    docker push "${full_img}":latest
fi
